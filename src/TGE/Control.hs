{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}

module TGE.Control
    ( Computation
    , runComputation
    ) where


import Control.Monad.IO.Class (MonadIO)
import Control.Monad.Writer (runWriterT)
import Control.Monad.State.Lazy (StateT(StateT), evalStateT, runStateT)
import Control.Monad.Except (MonadError, ExceptT)
import Control.Monad.Extra2.Exception (ExceptProgramExit, exitToIO)
import Control.Monad.Extra2.Logger (LoggerT, LogEntry, LogLevel, printLogs)
import Control.Monad.RWS (MonadReader(ask, local), MonadState(get), MonadWriter)


-- Newtype so that we can have the MonadReader instance read the state
newtype Computation s a = Computation ((StateT s) (ExceptT ExceptProgramExit (LoggerT IO)) a)
    deriving
      ( Functor
      , Applicative
      , Monad
      , MonadState s
      , MonadError ExceptProgramExit
      , MonadWriter [LogEntry]
      , MonadIO )


-- TODO Generalise into some readable-state wrapper type?
instance MonadReader s (Computation s) where
    ask = get
    local f (Computation m) = Computation . StateT $ \s -> runStateT m (f s)


-- To allow this to return results need to modify throwLoggedToIO to return Maybe values
runComputation :: LogLevel -> Computation s () -> s -> IO ()
runComputation logLevel (Computation computation) iniState = do
    (_, logs) <- (runWriterT . exitToIO . evalStateT computation) iniState
    printLogs logLevel logs

