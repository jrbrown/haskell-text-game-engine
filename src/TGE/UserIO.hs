{-# LANGUAGE FlexibleContexts #-}

module TGE.UserIO
    ( selectionUI
    , computationSelection
    , parseUserInput
    ) where


import Data.List (intercalate, nub)
import Data.List.Extra ((!?))
import Data.List.Extra2 (ulines)
import Data.Maybe (fromMaybe)

import Control.Monad.IO.Class (MonadIO, liftIO)
import Control.Monad.Extra2.Logger (MonadLogger, info)

import Text.Read (readMaybe)
import Text.Parsec (parse)
import Text.Parsec.Error (ParseError, Message (..), messageString, errorMessages)
import Text.Parsec.String (Parser)


mkNumberedList :: [String] -> String
mkNumberedList strings = ulines finalStrings
    where 
        indexStrings :: [(Integer, String)]
        indexStrings = zip [0..] strings
        finalStrings = map (\(x,y) -> show x ++ ". " ++ y) indexStrings


computationSelection :: MonadIO m => [m a] -> m a
computationSelection mutList = do
    user_in <- liftIO getLine
    fromMaybe reRequestInput (readMaybe user_in >>= (mutList !?))
        where
            reRequestInput = do
                let num = show (length mutList - 1)
                liftIO $ putStrLn $ "Invalid input, enter a number from 0 to " ++ num
                computationSelection mutList


selectionUI :: MonadIO m => [(String, m a)] -> m a
selectionUI choices = do
    let (descs, computations) = unzip choices
    liftIO $ putStrLn $ mkNumberedList descs ++ "\n"
    computationSelection computations


customParserError :: ParseError -> String
customParserError err = "Input not recognised\nTry one of: " ++ intercalate ", " exp_words ++ "\n"
    where exp_words = nub [messageString e | e <- errorMessages err, isExpect e]
          isExpect (Expect _) = True
          isExpect _          = False


parseUserInput :: (MonadIO m, MonadLogger m) => Parser a -> m a
parseUserInput parser = do
    user_in <- liftIO getLine
    case parse parser "User Input" user_in of
      Right x -> return x
      Left e  -> do
          liftIO $ putStrLn $ customParserError e
          info $ "Bad parse of user input, full error message:\n" ++ show e
          parseUserInput parser

