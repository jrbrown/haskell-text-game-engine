module Main
  ( main
  ) where


import TGE.Game (runGame)


main :: IO ()
main = runGame

