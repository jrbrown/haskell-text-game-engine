{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE OverloadedStrings #-}

module TGE.Player
    ( Player (..)
    , playerName, inventory
    , defaultPlayer
    ) where


import TGE.Item (ItemID)
import Control.Lens (makeLenses, (^.))

import qualified Data.Yaml as Y
import Data.Yaml (FromJSON, parseJSON, (.:?), (.!=))


data Player = Player
    { _playerName :: String
    , _inventory :: [ItemID] }

makeLenses ''Player

instance FromJSON Player where
    parseJSON (Y.Object v) =
        Player <$>
            v .:? "playerName" .!= (defaultPlayer^.playerName) <*>
            v .:? "inventory" .!= (defaultPlayer^.inventory)
    parseJSON _ = fail "Expected Object for Player value"


defaultPlayer :: Player
defaultPlayer = Player
    { _playerName = "Player"
    , _inventory = [] }

