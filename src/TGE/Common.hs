{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE FlexibleContexts #-}

module TGE.Common
    ( IDable (..)
    , WrappedText (..)
    , mkIndex
    , ParsedAction (..)
    , ExceptActionFailed (..)
    , FailableActT
    , actErrToFail
    , unwrapFailableAct
    , untilM
    , tryChoice
    , char', string'
    , stringChoice, stringChoice'
    , synonymChoice, synonymChoice'
    ) where


import TGE.Control (Computation)

import Control.Monad.IO.Class (MonadIO, liftIO)
import Control.Monad.Except (ExceptT, mapExceptT)
import Control.Monad.Extra2.Logger (MonadLogger, LogLevel (..), info)
import Control.Monad.Extra2.Exception (LoggableException (..), unwrapWithAct)

import Data.Map (Map, fromList)
import Data.Bifunctor (first)
import Data.Tuple.Extra (dupe)
import Data.Char (toLower, toUpper)

import Text.Parsec (char, string, try, choice, (<?>))
import Text.Parsec.String (Parser)

import Control.Applicative ((<|>))


class (Ord b) => IDable a b where
    getID :: a -> b

mkIndex :: (IDable a b) => [a] -> Map b a
mkIndex list = fromList $ map (first getID . dupe) list


class WrappedText a where
    txt :: a -> String


-- Not sure if specifying Computation here is best way to do things
-- Another method would probably require refactoring the whole ParsedAction thing into something else
-- Bool is to specify if action was succesful, if not we need to ask for a new one
-- TODO: Get rid of this, and use a more dynamic check on whether to re-print location data
class ParsedAction a s | a -> s where
    actionParser :: Parser a
    executeAction :: a -> Computation s Bool


data ExceptActionFailed = ExceptActionFailed
    { _desc :: String
    , _user_message :: String
    } deriving (Eq, Ord, Show)

instance LoggableException ExceptActionFailed where
    severity _ = LogInfo
    description = _desc

type FailableActT = ExceptT ExceptActionFailed


-- Helper functions for resolving things which might throw other errors

-- For serious things breaking - ideally should never happen
actErrToFail :: (MonadLogger m, LoggableException e, Show e) => ExceptT e m a -> FailableActT m a
actErrToFail = mapExceptT f
    where f errorableAct = do
              result <- errorableAct
              case result of
                Left err -> do
                    info $ "Converted error " ++ show err ++ " into ExceptActionFailed"
                    return (Left $ ExceptActionFailed
                        { _desc = description err
                        , _user_message = "The action you tried to take caused an error... (Check the logs!)" })
                Right x  -> return (Right x)


-- For more run-of-the-mill the-user-typed-the-wrong-thing errors
unwrapFailableAct :: (Show a, MonadIO m, MonadLogger m) => a -> FailableActT m Bool -> m Bool
unwrapFailableAct act_spec = unwrapWithAct handleError
    where handleError e = do
            (liftIO . putStrLn) (_user_message e)
            info ("Action " ++ show act_spec ++ " failed due to: " ++ description e)
            return False


-- TODO: Move to Extra2, though refactor will remove it
untilM :: (Monad m) => (a -> Bool) -> m a -> m a
untilM test act = act >>= (\x -> if test x then return x else untilM test act)


tryChoice :: [Parser a] -> Parser a
tryChoice = choice . fmap try


-- Primed versions are case insensitive

char' :: Char -> Parser Char
char' c = char (toLower c) <|> char (toUpper c)

string' :: String -> Parser String
string' = mapM char'

stringChoice :: [String] -> Parser String
stringChoice ss = tryChoice (string <$> ss)

stringChoice' :: [String] -> Parser String
stringChoice' ss = tryChoice (string' <$> ss)

synonymChoice :: [String] -> Parser String
synonymChoice [] = tryChoice []
synonymChoice c@(x:_) = stringChoice c <?> x

synonymChoice' :: [String] -> Parser String
synonymChoice' [] = tryChoice []
synonymChoice' c@(x:_) = stringChoice' c <?> x

