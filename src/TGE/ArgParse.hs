{-# LANGUAGE TemplateHaskell #-}

module TGE.ArgParse
    ( Config
    , logLevel
    , sourceFile
    , parseArgs
    ) where

import Control.Monad.Extra2.Logger (LogLevel (..))
import Control.Lens (makeLenses)

import Data.Char (toLower)
import Text.Read (readMaybe)

import Options.Applicative


data Config = Config
    { _logLevel :: LogLevel 
    , _sourceFile :: String }
    deriving (Eq, Ord, Show)

makeLenses ''Config


configP :: Parser Config
configP = Config
      <$> logLevelP
      <*> sourceFileP


logLevelP :: Parser LogLevel
logLevelP = option (maybeReader readLogLevel)
                (  long "log_level"
                <> short 'l'
                <> metavar "LOGLEVEL"
                <> help "Logging level"
                <> value LogInfo
                <> showDefault )


readLogLevel :: String -> Maybe LogLevel
readLogLevel arg = case readMaybe arg of
                     Just x -> Just x
                     Nothing -> readLogLevel' $ fmap toLower arg

readLogLevel' :: String -> Maybe LogLevel
readLogLevel' "debug" = Just LogDebug
readLogLevel' "d" = Just LogDebug
readLogLevel' "info" = Just LogInfo
readLogLevel' "i" = Just LogInfo
readLogLevel' "warn" = Just LogWarn
readLogLevel' "w" = Just LogWarn
readLogLevel' "error" = Just LogError
readLogLevel' "err" = Just LogError
readLogLevel' "e" = Just LogError
readLogLevel' "critical" = Just LogCritical
readLogLevel' "c" = Just LogCritical
readLogLevel' _ = Nothing


sourceFileP :: Parser String
sourceFileP = strOption
              (  long "source" 
              <> short 's'
              <> metavar "FILENAME"
              <> help "File to load game data from" )


argParser :: ParserInfo Config
argParser = info (configP <**> helper)
            (  fullDesc
            <> progDesc "Load up game data from yaml and launch a text based game."
            <> header "Haskell Text Game Engine" )


parseArgs :: IO Config
parseArgs = execParser argParser

