{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE GeneralisedNewtypeDeriving #-}

module TGE.Item
    ( ItemIndex
    , ItemID (..)
    , PickupData (..)
    , Item (..)
    , itemID, itemName, itemDescription, pickupData
    ) where


import TGE.Common (IDable (..), WrappedText (..))

import qualified Data.Yaml as Y
import Data.Yaml (FromJSON, parseJSON, (.:), (.:?))
import Data.Aeson (FromJSONKey)
import Data.Map (Map)

import Control.Lens (makeLenses, (^.))


type ItemIndex = Map ItemID Item

newtype ItemID = ItemID String deriving (Eq, Ord, Show, FromJSON, FromJSONKey)

instance WrappedText ItemID where
    txt (ItemID str) = str


-- An items uses are determined by what data it has, i.e. if it has WeaponData it can be used as a
-- weapon, if there's a Nothing then it can't


newtype PickupData = PickupData
    { pickupableInfinite :: Bool }
    deriving (Eq, Ord, Show)

instance FromJSON PickupData where
    parseJSON (Y.Object v) = PickupData <$> v .: "pickupableInfinite"
    parseJSON _ = fail "Expected Object for PickupData value"


data Item = Item
    { _itemID :: ItemID
    , _itemName :: String
    , _itemDescription :: String
    , _pickupData :: Maybe PickupData}


makeLenses ''Item


instance FromJSON Item where
    parseJSON (Y.Object v) =
        Item <$>
            v .: "itemID" <*>
            v .: "itemName" <*>
            v .: "itemDescription" <*>
            v .:? "pickupData"
    parseJSON _ = fail "Expected Object for Item value"

instance IDable Item ItemID where
    getID = _itemID

instance Eq Item where
    (==) x y = x^.itemID == y^.itemID

instance Ord Item where
    (<=) x y = x^.itemID <= y^.itemID

instance Show Item where
    show = show . _itemID

