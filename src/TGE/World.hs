{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE GeneralisedNewtypeDeriving #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE ScopedTypeVariables #-}

module TGE.World
    ( WorldData (..)
    , MonadReadWorld
    , MonadWriteWorld
    , locationIndex, currentLocation, itemIndex, player
    , LocationID (..)
    , Location (..)
    , LocationIndex
    , LocAction
    , getCurrentLoc
    , locInfoStr
    , newLocationData
    , verifyWorld
    ) where


import TGE.Common
    ( IDable (..), WrappedText (..), ParsedAction (..), ExceptActionFailed (..), FailableActT
    , actErrToFail, unwrapFailableAct, tryChoice, synonymChoice')
import TGE.Player (Player, inventory, defaultPlayer)
import TGE.Item ( ItemIndex, Item, ItemID (..), _pickupData, pickupableInfinite, _itemDescription)

import qualified Data.Yaml as Y
import Data.Yaml (FromJSON, parseJSON, (.:), (.:?), (.!=))
import Data.Aeson (FromJSONKey)
import Data.Map (Map, insert, lookup)
import Data.List (intercalate, deleteBy)

import Text.Parsec (spaces, many1, alphaNum, anyToken, (<?>))
import Text.Parsec.String (Parser)

import Control.Monad.Trans (lift)
import Control.Monad.Except (MonadError, throwError, ExceptT)
import Control.Monad.Extra2.Exception
    ( throwErrorLogged, lookupLE
    , ExceptGeneric (..), ExceptKeyNotFound (..) )
import Control.Monad.Extra2.Logger (MonadLogger, debug)
import Control.Lens (makeLenses, use, view, (^.), (%=), (.=), (.~))
import Control.Monad.RWS (MonadReader, MonadState, liftIO)

--import System.IO.Extra2 (printGet)


data WorldData = WorldData
    { _locationIndex :: LocationIndex
    , _currentLocation :: LocationID
    , _itemIndex :: ItemIndex
    , _player :: Player }

instance FromJSON WorldData where
    parseJSON (Y.Object v) =
        WorldData <$>
            v .: "locationIndex" <*>
            v .: "currentLocation" <*>
            v .: "itemIndex" <*>
            v .:? "player" .!= defaultPlayer
    parseJSON _ = fail "Expected Object for WorldData value"


type MonadReadWorld m = MonadReader WorldData m
type MonadWriteWorld m = MonadState WorldData m


newtype LocationID = LocationID String deriving (Eq, Ord, Show, FromJSON, FromJSONKey)

instance WrappedText LocationID where
    txt (LocationID str) = str

data Location = Location
    { _locID :: LocationID
    , _preposition :: String
    , _locName :: String
    , _locDescription :: String
    , _connectingLocations :: [(String, LocationID)]
    , _locItems :: [(String, ItemID)]
    } deriving Show

instance FromJSON Location where
    parseJSON (Y.Object v) =
        Location <$>
            v .: "locID" <*>
            v .:? "preposition" .!= "in a" <*>
            v .: "locName" <*>
            v .: "locDescription" <*>
            v .:? "connectingLocations" .!= [] <*>
            v .:? "locItems" .!= []
    parseJSON _ = fail "Expected Object for WorldData value"


type LocationIndex = Map LocationID Location


makeLenses ''WorldData
makeLenses ''Location


instance IDable Location LocationID where
    getID = _locID

instance Eq Location where
    (==) x y = x^.locID == y^.locID

instance Ord Location where
    (<=) x y = x^.locID <= y^.locID


data LocAction = Move LocationID
               | ItemAct ItemAction
               deriving (Eq, Ord, Show)


instance ParsedAction LocAction WorldData where
    actionParser = tryChoice [ synonymChoice' ["go", "move"] >> spaces >> Move <$> locIDParser
                             , ItemAct <$> actionParser ]

    executeAction act_spec@(Move loc_id) = do
        unwrapFailableAct act_spec $ do
            currentLoc <- actErrToFail getCurrentLoc
            if loc_id `elem` (snd <$> currentLoc^.connectingLocations)
               then currentLocation .= loc_id >> return True
               else throwError $ ExceptActionFailed
                   { _desc = show loc_id ++ " not present at " ++ show currentLoc
                   , _user_message = "You're not sure how to get to \"" ++ txt loc_id ++ "\" from here... (Note location id's are currently case senstive)" }

    executeAction (ItemAct act) = executeAction act


data ItemAction = Examine ItemID
                | Pickup ItemID
                deriving (Eq, Ord, Show)


instance ParsedAction ItemAction WorldData where
    actionParser = tryChoice [examineParser, pickupParser]

    executeAction act_spec@(Examine item_id) = do
        unwrapFailableAct act_spec $ actIfItemAtCurrentLoc item_id $ do
            actErrToFail $ getItemData item_id >>= liftIO . putStrLn . _itemDescription
            return False

    executeAction act_spec@(Pickup item_id) = do
        unwrapFailableAct act_spec $ actIfItemAtCurrentLoc item_id $ do
            item <- actErrToFail $ getItemData item_id
            case _pickupData item of
              Nothing   -> (liftIO . putStrLn) ("You can't pickup " ++ txt item_id) >> return False
              Just pdat -> do
                  player.inventory %= (item_id :)
                  if pickupableInfinite pdat
                     then return ()
                     else actErrToFail $ use currentLocation >>= (`removeItem` item_id)
                  return False


locIDParser :: Parser LocationID
locIDParser = (LocationID <$> many1 anyToken) <?> "locationID"

examineParser :: Parser ItemAction
examineParser = synonymChoice' ["examine", "look", "look at", "inspect"] >> spaces
             >> Examine <$> itemIDParser

pickupParser :: Parser ItemAction
pickupParser = synonymChoice' ["take", "pickup", "pick", "grab"] >> spaces
            >> Pickup <$> itemIDParser

itemIDParser :: Parser ItemID
itemIDParser = (ItemID <$> many1 alphaNum) <?> "itemID"


actIfItemAtCurrentLoc :: (MonadReadWorld m, MonadLogger m)
                      => ItemID -> FailableActT m a -> FailableActT m a
actIfItemAtCurrentLoc item_id act = do
    location <- actErrToFail getCurrentLoc
    let items_ids_at_loc = _locItems location
    if item_id `elem` (snd <$> items_ids_at_loc)
        then act
        else throwError $ ExceptActionFailed
            { _desc = show item_id ++ " not present at " ++ show location
            , _user_message = "You can't seem to find \"" ++ show item_id ++ "\"..." }


getCurrentLoc :: (MonadReadWorld m, MonadLogger m, MonadError ExceptKeyNotFound m) => m Location
getCurrentLoc = do
    currentLocID <- view currentLocation
    view locationIndex >>= lookupLE currentLocID


getItemData :: (MonadWriteWorld m, MonadLogger m, MonadError ExceptKeyNotFound m) => ItemID -> m Item
getItemData item_id = use itemIndex >>= lookupLE item_id

newLocationData :: (MonadWriteWorld m) => Location -> m ()
newLocationData ldat = locationIndex %= insert (ldat^.locID) ldat


atLocStr :: Location -> String
atLocStr loc = unwords ["You are", loc^.preposition, loc^.locName]

locInfoStr :: Location -> String
locInfoStr currentLoc = intercalate "\n" ["\n" ++ replicate 90 '=', atLocStr currentLoc, desc]
    where desc = unwords [currentLoc^.locDescription, connecting_descs, item_descs]
          connecting_descs = unwords (fst <$> currentLoc^.connectingLocations)
          item_descs = unwords (fst <$> currentLoc^.locItems)


removeItem :: (MonadWriteWorld m, MonadLogger m, MonadError ExceptKeyNotFound m)
           => LocationID -> ItemID -> m ()
removeItem loc_id item_id = do
    loc <- use locationIndex >>= lookupLE loc_id
    let newLocItems = deleteBy (\t i -> snd t == snd i) ("", item_id) (loc^.locItems)
    newLocationData ((locItems .~ newLocItems) loc)


verifyIndex :: forall m. (MonadReadWorld m, MonadLogger m, MonadError ExceptKeyNotFound m)
            => m [LocationID]
verifyIndex = do
    startLoc <- getCurrentLoc
    f [] [startLoc]
    where f :: [LocationID] -> [Location] -> m [LocationID]
          f _ [] = return []
          f checkedIDs (loc:locsToCheck) = do
              locIndex <- view locationIndex
              let locIDs = map snd (loc^.connectingLocations)
                  checkResults = [ (x, Data.Map.lookup x locIndex)
                                 | x <- locIDs, x `notElem` checkedIDs]
                  newNotFound = [x | (x, Nothing) <- checkResults]
                  newCheckedIDs = [x | (x, Just _) <- checkResults]
                  newLocsToCheck = [l | (_, Just l) <- checkResults]
              futureNotFound <- f (checkedIDs ++ newCheckedIDs ++ newNotFound) (locsToCheck ++ newLocsToCheck)
              debug $ "World index verification, checked " ++ show (loc^.locID)
                      ++ ", found " ++ show newNotFound
              return (newNotFound ++ futureNotFound)


-- For checking properties about our world to help prevent later errors
verifyWorld :: (MonadReadWorld m, MonadLogger m, MonadError ExceptKeyNotFound m)
            => ExceptT ExceptGeneric m ()
verifyWorld = do
    indexCheckResult <- lift verifyIndex
    case indexCheckResult of
        [] -> return ()
        ls -> let locString = intercalate ", " (map (\(LocationID l) -> show l) ls)
               in throwErrorLogged (ExceptGeneric ("The following locations weren't found in index: " ++ locString))

