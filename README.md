# Haskell Text Game Engine


## To run
* Run the supplied shell script `build_and_launch_test_town.sh` or do the following
* `stack build` to compile
* `stack exec text-game-engine-exe -- -s test_town.yaml` to run with the test_town game


## Todo

### Package
* Write proper README
* AUR package (with pre-compiled binary?)

### Features
* Help menu (with more detail on available actions?)
    * What can I see ("look/find items"?): Show all interactable items?
    * Where can I go ("look locations/travel"?): Show all locations available for travel

### Technical
* More logging of things internally
* Remove invariant of yaml key and ID
    * Split parsing into new file(s) to reduce clutter
    * Parse into intermediate type which doesn't have ID info
    * Turn this into consistently id'd version of current types
    * OR
    * Find a way to not need id info in entry
        * Might open up some new cool things
        * Might make other stuff harder
        * Seems actually maybe possible
        * Yeah do this
* New model of locations and interactions where locations are built up of interactable elements which are described and presented for interaction, should allow factorisation of location-changing actions to be specific changes of these component objects, thus easier to encode in yaml
* Utilise ReaderT more for locally modifiable, globally immutable, parts of State?

