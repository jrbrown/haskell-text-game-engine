{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleContexts #-}

module TGE.Game
    ( GameData (..)
    , runGame
    ) where


import TGE.Common (ParsedAction (..), untilM, tryChoice, synonymChoice')
import TGE.World (MonadWriteWorld, WorldData, LocAction, verifyWorld, getCurrentLoc, locInfoStr, player)
import TGE.Player (inventory)
import TGE.UserIO (parseUserInput, selectionUI)
import TGE.Control (Computation, runComputation)
import TGE.ArgParse (parseArgs, logLevel, sourceFile)

import qualified Data.Yaml as Y
import Data.Yaml (FromJSON, parseJSON, (.:), decodeThrow)
import Data.List.Extra2 (ulines)
import Data.ByteString.Char8 (pack)

import Text.Parsec.String (Parser)

import Control.Lens (makeLenses, (^.), use)
import Control.Monad.IO.Class (MonadIO, liftIO)
import Control.Monad.Except (MonadError)
import Control.Monad.Extra2.Logger (MonadLogger, debug, info, warn, err, critical)
import Control.Monad.Extra2.Exception
    ( ExceptGeneric (..), ExceptProgramExit (..)
    , throwErrorLogged, unwrapThrowToIOLoggedAndExit, unwrapThrowToIO )



data GameData = GameData
    { _introText :: String
    , _initialWorld :: WorldData }

makeLenses ''GameData

instance FromJSON GameData where
    parseJSON (Y.Object v) =
        GameData <$>
            v .: "introText" <*>
            v .: "initialWorld"
    parseJSON _ = fail "Expected Object for GameData value"


data MainAction = Exit
                | Debug
                | ShowInventory
                | LocAct LocAction

instance ParsedAction MainAction WorldData where
    actionParser = tryChoice
        [ synonymChoice' ["exit", "quit"] >> return Exit
        , synonymChoice' ["debug"] >> return Debug
        , synonymChoice' ["inventory", "inv"] >> return ShowInventory
        , LocAct <$> actionParser ]
    executeAction Exit = throwErrorLogged GoodExit
    executeAction Debug = debugUI
    executeAction ShowInventory = playerShowInv >> return False
    executeAction (LocAct act) = executeAction act


playerShowInv :: (MonadIO m, MonadWriteWorld m) => m ()
playerShowInv = use (player.inventory) >>= liftIO . putStrLn . ulines . fmap show


customLogEntry :: (MonadIO m, MonadLogger m) => m ()
customLogEntry = do
    liftIO $ putStrLn "Select a log level\n"
    logFunc <- selectionUI [ ("Debug", return debug)
                           , ("Info", return info)
                           , ("Warn", return warn)
                           , ("Error", return err)
                           , ("Critical", return critical) ]
    liftIO $ putStrLn "Type log message:"
    logMsg <- liftIO getLine
    logFunc logMsg


debugUI :: (MonadIO m, MonadLogger m, MonadError ExceptProgramExit m) => m Bool
debugUI = do
    liftIO . putStrLn $ "\n" ++ replicate 90 '-'
    liftIO $ putStrLn "Debug Menu\n"
    selectionUI [ ("Throw an error", unwrapThrowToIO throwTestError >> debugUI)
                , ("Write log entry", customLogEntry >> debugUI)
                , ("Exit debug menu", return True)
                , ("Exit program", throwErrorLogged GoodExit) ]


throwTestError :: (MonadLogger m, MonadError ExceptGeneric m) => m ()
throwTestError = throwErrorLogged $ ExceptGeneric "Hello, I'm a test error"


mainLoop :: Computation WorldData ()
mainLoop = do
    unwrapThrowToIOLoggedAndExit $ getCurrentLoc >>= (liftIO . putStrLn . locInfoStr)
    --See ParsedAction todo
    _ <- untilM id (do
        (liftIO . putStrLn) ""
        parseUserInput (actionParser :: Parser MainAction) >>= executeAction)
    mainLoop


game :: Computation WorldData ()
game = do
    info "Verifying world..."
    _ <- (unwrapThrowToIO . unwrapThrowToIO) verifyWorld
    info "World verification complete."
    info "Launching main loop..."
    mainLoop


runGame :: IO ()
runGame = do
    config <- parseArgs
    game_data <- readFile (config^.sourceFile) >>= (decodeThrow . pack)
    putStrLn $ game_data^.introText
    runComputation (config^.logLevel) game (game_data^.initialWorld)

